Name:           python-neotime
Version:        1.7.4
Release:        1
Summary:        Nanosecond resolution temporal types
License:        ASL 2.0
URL:            https://github.com/technige/neotime
Source0:        https://files.pythonhosted.org/packages/source/n/neotime/neotime-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%description
The neotime module defines classes for working with temporal data to nanosecond precision.
These classes comprise a similar set to that provided by the standard library datetime module.
Inspiration has also been drawn from ISO-8601.

Requires:       python3-pytz
Requires:       python3-six

%prep
%autosetup -n neotime-%{version}
find ./ -name '*.py' -exec sed -i '/^#!\/usr\/bin\/env python$/d' '{}' ';'

%build
%py3_build

%install
%py3_install

%files -n %{name}
%doc README.rst
%{python3_sitelib}/*

%changelog
* Wed Jul 21 2021 mrcangye <mrcangye@email.cn> - 1.7.4-1
- Initial release
